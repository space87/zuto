(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
function bannerClicker(clickedEle) {
    var clickedSlide = clickedEle.target.getAttribute('data-tab');
    var activeSlide = document.querySelector('.slide-'+clickedSlide);
    var slides = document.querySelectorAll('.slide');
    var slideCounter = document.querySelectorAll('.slideCount li');


    for(var x = 0; x < slideCounter.length; x++) {


        if(slideCounter[x].className.indexOf('active') > -1) {

            slideCounter[x].className = '';
        }

    }

    for(var i = 0; i < slides.length; i++) {

        if(slides[i].className.indexOf('active') > -1) {
            slides[i].className = 'slide slide-'+(i+1);
        }
    }

    activeSlide.className += ' active';
    clickedEle.target.className += ' active';
    activeSlide.style.display = 'inline-block;'
};

module.exports = bannerClicker;
},{}],2:[function(require,module,exports){
var bannerClicker = require('./banner');
var menu = require('./menu');


// start clicks listening on page load

$(document).ready(function() {

    $('.menuIcon').on('click', function() {
        menu.toggleMenu();
    });

    $('.subActive').on('click', function() {
        menu.toggleSubMenu(document.querySelector('.subMenu'));
    });

    $('.slideCount li').on('click', function(e) {
        bannerClicker(e)
    });


})





},{"./banner":1,"./menu":3}],3:[function(require,module,exports){
module.exports =  {
    toggleMenu: function() {

        var menu = document.querySelector('.mainNav');

        if(menu.getAttribute('data-menu') === 'open') {


            menu.style.transform = 'translate3d(200px,0,0)';
            menu.setAttribute('data-menu','close');
            document.querySelector('.menuIcon').style.transform = 'rotate(0deg)';

        } else {

            menu.style.transform = 'translate3d(-200px,0,0)';
            menu.setAttribute('data-menu','open');
            document.querySelector('.menuIcon').style.transform = 'rotate(90deg)';
        }


    },
    toggleSubMenu: function(element) {

        if($('html').width() < 768) {

            if(document.querySelector('.subActive').getAttribute('data-subMenu') === 'open') {
                element.style.display = 'none';
                document.querySelector('.subActive').setAttribute('data-submenu','close');

            } else {
                element.style.display = 'block';
                document.querySelector('.subActive').setAttribute('data-submenu','open');
            }
        } else {
            return true;
        }
    }


}
},{}]},{},[2])