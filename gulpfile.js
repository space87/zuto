// gulp Sependencies
var gulp = require('gulp');
var gutil = require('gulp-util');
var rename = require('gulp-rename');
var browserSync = require('browser-sync');
var plumber = require('gulp-plumber');
var watch = require('gulp-watch');
var reload = browserSync.reload;
var clean = require('gulp-clean');
var filesize = require('gulp-filesize');
var del = require('del');


// Image Dependencies
var imagemin = require('gulp-imagemin');
var pngQuant = require('imagemin-pngquant');
var jpegtran = require('imagemin-jpegtran');


// Style Dependencies
var sass = require('gulp-ruby-sass');
var autoprefixer = require('gulp-autoprefixer');
var cssMinify = require('gulp-minify-css');

// Script Dependencies
var babelify = require('babelify');
var browserify = require('gulp-browserify');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var mochaPhantomJS = require('gulp-mocha-phantomjs');

// Paths
var localPath = './local/';
var sourcePath = './src/';
var buildPath = './build/';
var liveURL = '//media.ao.com/uk/cmsimages/brandPages/nick/';



// compling tasks
gulp.task('sass', function () {
    return sass(sourcePath+'/sass/')
        .pipe(plumber())
        .on('error', function (err) {
            console.error('Error!', err.message);
        })
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cssMinify())
        .pipe(gulp.dest(localPath+'/css/'))
        .pipe(browserSync.stream());

});

gulp.task('scripts', function () {
    return gulp.src(sourcePath+'/js/base.js')
        .pipe(browserify({
            insertGlobals : false,
            debug : false
        }))
        .pipe(uglify())
        .pipe(gulp.dest(localPath+'/js/'))
        .pipe(browserSync.stream());
});


gulp.task('html',function() {
    return gulp.src(sourcePath+'/*.html')
        .pipe(gulp.dest(localPath))
        .pipe(browserSync.stream());
});



// local dev tasks

gulp.task('png',function() {
    return gulp.src(sourcePath+'/images/*.png')
        .pipe(imagemin({
            optimizationLevel: 3,
            use: [pngQuant()]
        }))

        .pipe(gulp.dest(localPath+'/images/'))
        .pipe(browserSync.stream());
});

gulp.task('jpg',function() {
    return gulp.src(sourcePath+'/images/*.jpg')
        .pipe(imagemin({
            progressive: true,
            use: [jpegtran()]
        }))
        .pipe(gulp.dest(localPath+'/images/'))
        .pipe(browserSync.stream());
});



gulp.task('browser-sync',function() {
    browserSync({
        server: {
            baseDir: "./local"
        },
        logLevel: "debug",
        logPrefix: "Nick Fury",
        open: "ui"
    });

});



// build steps


gulp.task('browserify-test', function() {
    return gulp.src('./test/index.js')
        .pipe(browserify({
            insertGlobals: false
        }))
        .pipe(rename('page-test.js'))
        .pipe(gulp.dest('./test/build'));
});

gulp.task('test', ['browserify-test'], function() {
    return gulp.src('./test/runner.html')
        .pipe(mochaPhantomjs());
});




//watching tasks
gulp.task('watch',function() {
    gulp.watch("./src/sass/**/**/*.scss", ['sass']);
    gulp.watch("./src/images/**/*.jpg", ['jpg']);
    gulp.watch("./src/images/**/*.png", ['png']);
    gulp.watch("./src/js/**/*.js", ['scripts']);
    gulp.watch("./src/*.html", ['html']);
    gulp.watch('./test/index.js', ['browserify-test', 'test']);
    gulp.watch(['./local/*.html', './local/js/*.js','./local/css/*.css', './local/images/*']).on('change', browserSync.reload);
});

// check images and minify
gulp.task('images',['jpg','png']);

// assign local to gulp
gulp.task('default',['local']);

// compile and place files in local folder and fire up browser sync
gulp.task('local', ['images', 'scripts', 'sass', 'html', 'browser-sync','watch']);


// test the page
gulp.task('test',['test']);



