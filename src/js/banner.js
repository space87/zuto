function bannerClicker(clickedEle) {
    var clickedSlide = clickedEle.target.getAttribute('data-tab');
    var activeSlide = document.querySelector('.slide-'+clickedSlide);
    var slides = document.querySelectorAll('.slide');
    var slideCounter = document.querySelectorAll('.slideCount li');


    for(var x = 0; x < slideCounter.length; x++) {


        if(slideCounter[x].className.indexOf('active') > -1) {

            slideCounter[x].className = '';
        }

    }

    for(var i = 0; i < slides.length; i++) {

        if(slides[i].className.indexOf('active') > -1) {
            slides[i].className = 'slide slide-'+(i+1);
        }
    }

    activeSlide.className += ' active';
    clickedEle.target.className += ' active';
    activeSlide.style.display = 'inline-block;'
};

module.exports = bannerClicker;