module.exports =  {
    toggleMenu: function() {

        var menu = document.querySelector('.mainNav');

        if(menu.getAttribute('data-menu') === 'open') {


            menu.style.transform = 'translate3d(200px,0,0)';
            menu.setAttribute('data-menu','close');
            document.querySelector('.menuIcon').style.transform = 'rotate(0deg)';

        } else {

            menu.style.transform = 'translate3d(-200px,0,0)';
            menu.setAttribute('data-menu','open');
            document.querySelector('.menuIcon').style.transform = 'rotate(90deg)';
        }


    },
    toggleSubMenu: function(element) {

        if($('html').width() < 768) {

            if(document.querySelector('.subActive').getAttribute('data-subMenu') === 'open') {
                element.style.display = 'none';
                document.querySelector('.subActive').setAttribute('data-submenu','close');

            } else {
                element.style.display = 'block';
                document.querySelector('.subActive').setAttribute('data-submenu','open');
            }
        } else {
            return true;
        }
    }


}